/*Learning objectives:
	-create code that will execute only when a given condition has been made

Topics:
	-Assignment operators
	-conditional statement
	-Ternary operator


//Lesson proper
Conditional statements
	-one of the key features of a programming language

//Basic assignment operatior (=)

	let variable = "Initial value";
*/
//Mathematical operator

	let num1 = 5;
	let num2 = 10
	let num3 = 4;
	let num4 = 40;

//Addition assignment
	/*
	Addition
		Left operand
		right operand
	*/

	let sum = num1 + num4;

	// num1 = num1 + num4;
	num1 += num4;
	console.log(num1);

	num2 += num3;
	console.log(num2);

	num1 += 55;
	console.log(num1);
	console.log(num4);

	let string1 = "Boston";
	let string2 = "Celtics";

	string1 += string2;
	console.log(string1);

	console.log(string2);

	/*15 += num1;
	console.log(15); */

	//Subtruction Operator
	num1 -= num2;
	console.log("Result:" + num1);

	//Multipliction Operator
	num2 *= num3;
	console.log("Result:" + num2);

	//Division-multiplication
	num4 /= num3;
	console.log("Result: " + num4);

	// Arithmitic Operators
	let x = 1397;
	let y = 7831;

	sum = x + y;
	console.log("Result: " + sum);

	let difference = y - x;
	console.log("Result: " + difference);

	let product = x * y;
	console.log("Result: " + product);

	let quotient = y / x;
	console.log("Result: " + quotient);

	let remainder = y%x;
	console.log("Result: " + remainder);

	//Multiple Operators and Parentheses
	/*
	-when multiple operators are applied in a single statemnt
	-PEMDAS
	*/

	let mdas = 1+2-3*4/5;
	console.log(mdas);

	let pemdas = 1+(2-3)*(4/5);
	console.log(pemdas);

	pemdas = (1+(2-3))*(4/5);
	console.log(pemdas);

	//Increment and Decrement

	let z = 1;
	++z;
	console.log(z);

	z++;
	console.log(z);
	console.log(z++);
	console.log(z);
//Prefix vs Postfix
	console.log(z);
	console.log(z++);
	console.log(z);

	console.log(++z);
	//Decrementation
	console.log(--z);
	console.log(z--);
	console.log(z);

	//Comparison Operators

	console.log ( 1==1);//True

	let isSame = 55 == 55;
	console.log(isSame);

	console.log(1 == '1');
	console.log(0 == false);
	console.log(1 == true);
	console.log(true == "true");

	let name1 = "Jin";
	let name2 = "Jimin";
	let name3 = "Jungkook";
	let name4 = "V";

	let number1 = 50;
	let number2 = 60;
	let numString1 = "50";
	let numString2 = "60";

	console.log(numString1 == number1);
	console.log(numString1 === number1);
	console.log(numString1 != number1);
	console.log(name4 !== "num3");
	console.log(name3 == "Jungkook");
	console.log(name1 == "Jin");

	/* Relational comparison operators
	a comparison operator compares its operand and returns a boolean value based on whether the comparison is true>

*/

	let a = 500;
	let b = 700;
	let c = 8000;
	let numString3 = "5500";

	// Greater than (>)
	console.log(a > b);
	console.log(c > y);

	//authorization

	let isAdmin = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered;
	console.log(authorization1);

	let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2);

	let requiredLevel = 95;
	let requiredAge =18;
	let authorization3 = isRegistered && requiredLevel === 25;
	console.log(authorization3);

	let authorization4 = isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization4);

	//User Info

	let userName = "gamer2001";
	let userName2 = "shadow1991";
	let userAge = 15;
	let userAge2 = 30;

	let registration1 = userName.length > 8 && userAge >= requiredAge;

	console.log(registration1);

	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;

	console.log(registration2);

	let userLevel = 100;
	let userLevel2 = 65;

	let guildRequirement1 = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
	console.log(guildRequirement1);


	let guildAdmin = isAdmin&& userLevel2>= requiredLevel;
	console.log(guildAdmin);

	let guildAdmin2 = !isAdmin && userLevel2>= requiredLevel;
	console.log(guildAdmin2);

	console.log(!isRegistered);

	//If-else
	let userName3 = "crusader_1993";
	let userLevel3 = 25;
	let userAge3 = 20;

	/*
	if(true){
		alert("We just run an if condition!")
	}
	*/

	if(userName3.length > 10){
		console.log("welcome to game online");
	}

	if(userLevel3 >= requiredLevel){
		console.log("You are qualified to join the guild");
	}

	//else
	if(userName3.length > 0 && userLevel3 <= 25 && userAge3 >= requiredAge){
		console.log("Thank you for joining the Noobies Guild");
	}
	else{
		console.log("You are too strong to be a noob. :(");
	}

	//else if

	if(userName3.length >= 10 && userLevel3 <= 25 && userAge >= requiredAge){
		console.log("Thank you for joining.");
	}
	else if(userLevel3 > 25){
		console.log("You are too strong to be a noob.");
	}
	else if(userAge3 < requiredAge){
		console.log("You are too young to join");
	}
	else if(userName3.length < 10){
		console.log("Username too short.");
	}

	//if-else in function
	function addNum(num1, num2){
		if(typeof num === 'number' && typeof num2 === 'number'){
			console.log("Run only if both arguments passed are numbers.")
			console.log(num1 + num2);
		}
		else{
			console.log("One or both of the arguments are not numbers.");
		} 
		
	}

addNum(5,2);
	

/*Mini activity
	-add another condition to our nested if statement
	-check if password is 8 characters long or more
	-add else statement which will run if both conditions are not met
	-show an alert which says "Credentials are too short."

	Stretch Goals:
		-add an else if statement if username is less than 8
		-alert that username is too short

		-add an else if statement if the password is 8 characters or less

*/

function login(username, password){
		if(typeof username === 'string' && typeof password === 'string'){
			console.log("Both arguments are strings");
		}
		if(username.length >= 8 && password.length >= 8)
			{
				console.log("logging in")
			} 
			else if (password.length < 8)
			{
				console.log(alert("password is too short"))
			}
			else if (username.length < 8)
			{
				console.log(alert("username is too short"))
			}
			else {
				console.log(alert("Credentials are too short"));
			}

	}

	login('Skills999','123456789');

/*
Mini Act 2
Create a function that will determine the color of the shirt te user will wear depending on the inputted day.
Black - mon
Green - tue
Yellow - wed
Red - thu
Violet - fri
Blue - sat
White - sun
*/

function shirtColor(Day){
	let day = Day.toLowerCase();
	let colorShirt = "";

	if(typeof day === 'string'){
			console.log("The argument is a string")
		}

	else{console.log(alert("Please input a proper string"));}

	if(day == "monday"){
		colorShirt = "Black"
		console.log(alert("Today is " + day + " Wear " + colorShirt))
	}

	else if(day == "tuesday"){
		colorShirt = "Green"
		console.log(alert("Today is " + day + " Wear " + colorShirt))
	}

	else if(day == "wednesday"){
		colorShirt = "Yellow"
		console.log(alert("Today is " + day + " Wear " + colorShirt))
	}

	else if(day == "thursday"){
		colorShirt = "Red"
		console.log(alert("Today is " + day + " Wear " + colorShirt))
	}

	else if(day == "friday"){
		colorShirt = "Violet"
		console.log(alert("Today is " + day + " Wear " + colorShirt))
	}

	else if(day == "saturday"){
		colorShirt = "Blue"
		console.log(alert("Today is " + day + " Wear " + colorShirt))
	}

	else if(day == "sunday"){
		colorShirt = "White"
		console.log(alert("Today is " + day + " Wear " + colorShirt))
	}

	else{console.log(alert("Inputs invalid"));
	}
}

shirtColor("TuesDaY");

/*Switch Statement
	- is used an alternative to an if, else-if or else tree, where the data being evaluated or checked in an expected input
	- if we want to select one of many code blocks/statements to be executed
	syntax:
		switch(expression/condition){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
*/
let hero = "Anpanman";
switch(hero){
	case "Jose Rizal":
       console.log("Philippines National Hero");
       break;
    case "George Washington":
        console.log("Hero of the American Revolution");
        break
    case "Hercules":
         console.log("Legendary Hero of the Greek");
         break;
    case "Anpanman":
         console.log("Superhero!");
         break;
};

function roleChecker(role){
	switch(role){
		case "Admin":
		    console.log("Welcome Admin, to the Dashboard.");
		    break;
		case "User":
		    console.log("You are not authorized to view this page.");
		    break;
		case "Guest":
		     console.log("Go to the registration page to register.");
		    break;
		    // break it breaks/terminates the code block. If this was not added to your case then, the next case will run as well.
		default:
		// by default your switch ends with default case, so therefore,even if there is no break keyword in your default case, it will not run anything else
		    console.log("Invalid Role.") 
	};
};

roleChecker("Admin");

/*
	Mini-Activity
	  create a colorOfTheDay function, instead of using if-else, convert it to a switch(use switch statement)
 */

// function with if-else and return
function gradeEvaluator(grade){
	/*
		evaluate the grade input and return the letter distinction
			 - if the grade is less than or equal to 70 = F
			 - if the grade is greater than or equal to 71 = C
			 - if the grade is greater than or equal to 80 = B
			 - if the grade is greater than or equal to 90 = A
		resume: 3:45 PM	
	*/
	if(grade >= 90){
		/*return keyword can be used in an if-else statement inside a function
			- it allows the function to return a value
		*/
		return "A";
	} else if(grade >= 80){
		return "B";
	} else if(grade >= 71){
		return "C";
	} else if(grade <= 70){
		return "F";
	} else {
		return "Invalid"
	};
};

let grade = gradeEvaluator(75);
console.log(`The result is: ${grade}`);

/* Ternary Operators*/
	// shorthand way of writing if-else statements
	/*
		syntax:
		condition ? if-statement : else statement
	 */
   
   
/*Activity*/
/* Instruction:


	Create two functions:

		-First function: oddEvenChecker
			-This function will check if the number input or passed as an argument is an odd or even number.
				-Check if the argument being passed is a number or not.
					-if it is a number, check if the number is odd or even.
					-even numbers are divisible by 2.
					-log a message in the console: "The number is even." if the number passed as argument is even.
					-log a message in the console: "The number is odd." if the number passed as argument is odd.
					-if the number passed is not a number type: 
						show an alert:
						"Invalid Input."

		-Second Function: budgetChecker()
			-This function will check if the number input or passed as an argument is over or is less than the recommended budget.
				-Check if the argument being is a number or not.
					-if it is a number check if the number given is greater than 40000
						-log a message in the console: ("You are over the budget.")
					-if the number is not over 40000:
						-log a message in the console: ("You have resources left.")
					-if the argument passed is not a number:
						-show an alert: ("Invalid Input.") 


Pushing Instructions:

Go to Gitlab:
	-in your projects folder and access your folder.
	-create s07  project folder 
	-untick the readme option
	-copy the git url from the clone button of your activity repo.

Go to Gitbash:
	-go to your s07 folder and access activity folder
	-initialize activity folder as a local repo: git init
	-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes repetition control activity"
	-push your updates to your online repo: git push origin master

Go to SB:
	-copy the url of the home page for your s07/activity repo (URL on browser not the URL from clone button) 
*/